# Репозиторий для поднятия инфраструктуры в Yandex Cloud

## Шаги выполнения

1. **Добавление необходимых переменных в Gitlab CI/CD variables**:

2. **Конфигурирование необходимых переменных**:
    - terraform/variables.tf - файл конфигурации terraform
    - terraform/terraformrc.config - файл для использования зеркала при установке yandex provider
    - terraform/modules/vm/cloud-config - cloud-config для настройки VM
    - ansible-provisioning/ansible.cfg - ansible конфиг
    - ci-files/.nfs-provisioner-install.yml - в переменных нужно указать ip сервера c nfd, и путь до shared nfs directory

3. **Запуск Pipeline для создания инфраструктуры**:
    1) s3-bucket-deploy
    2) k8s-deploy
    3) nfs-server-provision
    4) nfs-provisioner-install

4. **Запуск Pipeline для уничтожения инфраструктуры**:

    1) k8s-destroy
    2) s3-bucket-destroy

## Gitlab CI/CD vars

| Type     | Key                    | Description                                                                                                  |
|----------|------------------------|--------------------------------------------------------------------------------------------------------------|
| Variable | CLOUD_ID               | Yandex cloud ID. Used as a variable in the Terraform pipeline to identify the Yandex Cloud ID.               |
| Variable | FOLDER_ID              | Yandex folder ID. Used as a variable in the Terraform pipeline to specify the target folder in Yandex Cloud. |
| Variable | TOKEN                  | Masked Yandex token. Used as a variable in the Terraform pipeline to authenticate with the Yandex Cloud API. |
| FILE     | NFS_SERVER_PRIVATE_KEY | Masked Key for accessing the VM.                                                                             |

## Структура репозитория

```
.
|-- .gitlab-ci.yml                              Основной пайплайн
|-- README.md                            
|-- ansible-provisioning                        Ansible директория для настройки VM с nfs-server
|   |-- ansible.cfg 
|   |-- inventories
|   |   `-- prod
|   |       |-- group_vars
|   |       |   `-- all
|   |       |       `-- main.yaml
|   |       `-- hosts
|   |-- playbook.yaml
|   `-- roles
|       `-- nfs-server
|           |-- defaults
|           |   `-- main.yaml
|           `-- tasks
|               `-- main.yaml
|-- ci-files                                    Дочерние CI файлы, вызов которых происходит в корневом CI файле
|   |-- .k8s-deploy.yml                         CI для развертывания k8s кластера, VM, и развертывания Ingress и Cert-manager внуть кластера
|   |-- .k8s-destroy.yml                        Уничтожение инфраструктуры
|   |-- .nfs-provisioner-install.yml            CI для развертывания nfs-provisioner в k8s кластер
|   |-- .nfs-server-provision.yml               CI для запуска ansible роли по настроке VM с nfs-server
|   |-- .s3-bucket-deploy.yml                   Уничтожение инфраструктуры
|   `-- .s3-bucket-destroy.yml                  Уничтожение инфраструктуры
|-- k8s-manifests                               
|   |-- cert-manager
|   |   |-- cert-manager-deploy.yml             Манифест для развертывания cert-manager в k8s
|   |   `-- clusterIssuer-deploy.yml            Манифест для развертывания clusterIssuer в k8s
|   `-- ingress-nginx
|       `-- ingress-deploy.yml                  Манифиест для развертывания Ingress в k8s
`-- terraform                                   Директория для всех terraform файлов
    |-- main.tf
    |-- modules                                 Terraform модули
    |   |-- kubernetes                          Terraform модуль для k8s
    |   |   |-- k8s-cluster.tf
    |   |   |-- node-group.tf
    |   |   |-- outputs.tf
    |   |   |-- provider.tf
    |   |   |-- security-groups.tf
    |   |   |-- service-account.tf
    |   |   |-- variables.tf
    |   |   `-- yandex-key-managment-service.tf
    |   |-- network                             Terraform модуль для сети
    |   |   |-- main.tf
    |   |   |-- outputs.tf
    |   |   |-- provider.tf
    |   |   `-- variables.tf
    |   |-- static-address                      Terraform модуль для статического адреса
    |   |   |-- main.tf
    |   |   |-- outputs.tf
    |   |   |-- provider.tf
    |   |   `-- variables.tf
    |   `-- vm                                  Terraform модуль для VM
    |       |-- cloud-config                    Cloud-config для VM 
    |       |-- main.tf
    |       |-- outputs.tf
    |       |-- provider.tf
    |       `-- variables.tf
    |-- outputs.tf
    |-- provider.tf
    |-- variables.tf
    |-- terraformrc.config                      Конфиг для установки яндекс провайдера через зеркало
    |-- s3-backend                              Директория с terraform файлами для s3 backend
        |-- backend.conf                        Backend конфиг для дальнейшего использования его при развертывании основной инфраструктуры                       
        |-- main.tf
        |-- outputs.tf
        |-- provider.tf                 
        `-- variables.tf
```

## Screenshots

![Логотип](screenshots/app.png)
![Логотип](screenshots/files_on_nfs-server.png)
![Логотип](screenshots/module-pipeline.png)
![Логотип](screenshots/nfs-provisioner.png)
![Логотип](screenshots/yandex-cloud.png)


## Как это должно выглядеть (опускаем сетевые абстракции)
![Логотип](screenshots/shema.png)