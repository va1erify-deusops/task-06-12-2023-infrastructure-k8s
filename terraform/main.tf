module "network" {
  source          = "./modules/network"
  network_name    = var.network_name
  subnetwork_name = var.subnetwork_name
  cidr_v4         = var.cidr_v4
  zone            = var.zone
}

module "static-address" {
  source              = "./modules/static-address"
  depends_on          = [module.network]
  static_address_name = var.static_address_name
  deletion_protection = var.deletion_protection
  zone                = var.zone
}

module "nfs-server-vm" {
  source               = "./modules/vm"
  depends_on           = [module.network]
  subnetwork_id        = module.network.subnetwork_id
  name                 = var.vm_name
  internal_ip_address  = var.vm_internal_ip_address
  hostname             = var.vm_hostname
  platform             = var.vm_platform
  ram                  = var.vm_ram
  cpu                  = var.vm_cpu
  core_fraction        = var.vm_core_fraction
  boot_disk_image_id   = var.vm_boot_disk_image_id
  boot_disk_size       = var.vm_boot_disk_size
  boot_disk_type       = var.vm_boot_disk_type
  boot_disk_block_size = var.vm_boot_disk_block_size
}

module "kubernetes" {
  source     = "./modules/kubernetes"
  depends_on = [module.network, module.static-address]
  folder_id  = var.folder_id

  network_id                = module.network.network_id
  subnetwork_id             = module.network.subnetwork_id
  subnetwork_v4_cidr_blocks = module.network.subnetwork_v4_cidr_blocks
  subnetwork_zone           = module.network.subnetwork_zone

  k8_cluster_name           = var.k8_cluster_name
  k8_cluster_public_ip      = var.k8_cluster_public_ip
  k8_service_account_name   = var.k8_service_account_name
  k8s_node_group_name       = var.k8s_node_group_name
  kms_key_default_algorithm = var.kms_key_default_algorithm
  kms_key_name              = var.kms_key_name
  kms_key_rotation_period   = var.kms_key_rotation_period
  vpc_security_group_name   = var.vpc_security_group_name
  zone                      = var.zone

  boot_disk_size         = var.kubernetes_boot_disk_size
  boot_disk_type         = var.kubernetes_boot_disk_type
  container_runtime_type = var.kubernetes_container_runtime_type
  core_fraction          = var.kubernetes_core_fraction
  cores                  = var.kubernetes_cores
  nat                    = var.kubernetes_nat
  memory                 = var.kubernetes_memory
  platform_id            = var.kubernetes_platform_id
  preemptible            = var.kubernetes_preemptible
  scale_policy_initial   = var.kubernetes_scale_policy_initial
  scale_policy_max       = var.kubernetes_scale_policy_max
  scale_policy_min       = var.kubernetes_scale_policy_min
}