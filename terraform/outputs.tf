output "yandex_kubernetes_cluster_name" {
  value = module.kubernetes.yandex_kubernetes_cluster_name
}

output "vm_nfs_server_external_ip" {
  value = module.nfs-server-vm.external_ip
}