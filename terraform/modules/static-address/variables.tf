variable "static_address_name" {
  default = "default_static_address_name"
}

variable "deletion_protection" {
  default = "false"
}

variable "zone" {
  default = "ru-central1-a"
}