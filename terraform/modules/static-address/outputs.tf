output "static_address" {
  sensitive = false
  value     = yandex_vpc_address.static_address.external_ipv4_address[*].address
}
