resource "yandex_vpc_address" "static_address" {
  name                = var.static_address_name
  deletion_protection = var.deletion_protection
  external_ipv4_address {
    zone_id = var.zone
  }
}