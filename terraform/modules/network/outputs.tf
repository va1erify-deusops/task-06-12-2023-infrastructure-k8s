output "subnetwork_id" {
  value = yandex_vpc_subnet.subnetwork.id
}

output "network_id" {
  value = yandex_vpc_network.network.id
}

output "subnetwork_zone" {
  value = yandex_vpc_subnet.subnetwork.zone
}

output "subnetwork_v4_cidr_blocks" {
  value = yandex_vpc_subnet.subnetwork.v4_cidr_blocks
}