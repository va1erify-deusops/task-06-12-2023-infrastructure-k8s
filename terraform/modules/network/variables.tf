variable "network_name" {
  default = "default_network_name"
}
variable "subnetwork_name" {
  default = "default_subnetwork_name"
}
variable "cidr_v4" {
  default = "10.10.10.0/24"
}
variable "zone" {
  default = "ru-central1-a"
}