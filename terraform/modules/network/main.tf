resource "yandex_vpc_network" "network" {
  name = var.network_name
}

resource "yandex_vpc_subnet" "subnetwork" {
  name           = var.subnetwork_name
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = [var.cidr_v4]
  zone           = var.zone
}