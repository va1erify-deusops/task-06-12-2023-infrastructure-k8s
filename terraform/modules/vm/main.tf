resource "yandex_compute_instance" "vm" {
  name        = var.name
  platform_id = var.platform
  hostname    = var.hostname

  scheduling_policy {
    preemptible = true
  }

  resources {
    cores         = var.cpu
    memory        = var.ram
    core_fraction = var.core_fraction
  }

  boot_disk {
    initialize_params {
      image_id   = var.boot_disk_image_id
      size       = var.boot_disk_size
      type       = var.boot_disk_type
      block_size = var.boot_disk_block_size
    }
  }

#TODO: уже есть модуль, который создает статический IP, нужно передавать его сюда
  network_interface {
    subnet_id  = var.subnetwork_id
    nat        = true
    ip_address = var.internal_ip_address
  }

  metadata = {
    user-data = file("${path.module}/cloud-config")
  }
}

#TODO: добавить динамическое прокидывание ключа в cloud-config или вообще прокидывание самого cloud-config (лучше второе)