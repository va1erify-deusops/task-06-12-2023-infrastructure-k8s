# Ключ Yandex Key Management Service для шифрования важной информации,такой как пароли, OAuth-токены и SSH-ключи.
resource "yandex_kms_symmetric_key" "kms_key" {
  name              = var.kms_key_name
  default_algorithm = var.kms_key_default_algorithm
  rotation_period   = var.kms_key_rotation_period
}