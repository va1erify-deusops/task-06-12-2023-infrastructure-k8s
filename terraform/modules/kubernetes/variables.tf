variable "folder_id" {
  description = "Yandex folder id"
}

variable "k8_service_account_name" {
  default = "k8-default-service-account-name"
}

variable "k8_cluster_name" {
  default = "default-k8-cluster-name"
}

variable "k8_cluster_public_ip" {
  default = true
}

variable "zone" {
  default = "ru-central1-a"
}

variable "vpc_security_group_name" {
  default = "default_vpc_security_group__name"
}

variable "kms_key_name" {
  default = "default_kms_key_name"
}

variable "kms_key_default_algorithm" {
  default = "AES_128"
}

variable "kms_key_rotation_period" {
  default = "8760h"
  description = "1 year = 8760h"
}

variable "k8s_node_group_name" {
  default = "default-k8s-node-group-name"
}

variable "platform_id" {
  default = "standard-v3"
}

variable "nat" {
  default = true
}

variable "memory" {
  default = 4
}

variable "cores" {
  default = 2
}

variable "core_fraction" {
  default = 20
}

variable "boot_disk_type" {
  default = "network-hdd"
}

variable "boot_disk_size" {
  default = 30
}

variable "preemptible" {
  default = false
}

variable "container_runtime_type" {
  default = "containerd"
}

variable "scale_policy_min" {
  default = 1
}
variable "scale_policy_max" {
  default = 2
}
variable "scale_policy_initial" {
 default = 1
}

variable "subnetwork_id" {}
variable "network_id" {}
variable "subnetwork_zone" {}
variable "subnetwork_v4_cidr_blocks" {}