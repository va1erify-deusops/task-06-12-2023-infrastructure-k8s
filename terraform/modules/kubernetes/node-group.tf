resource "yandex_kubernetes_node_group" "k8s_node_group" {
  cluster_id = yandex_kubernetes_cluster.k8_cluster.id
  name       = var.k8s_node_group_name

#TODO: Добавить нормальный label
  labels = {
    "key" = "value"
  }

  instance_template {
    platform_id = var.platform_id
    network_interface {
      nat                = var.nat
      subnet_ids         = ["${var.subnetwork_id}"]
      security_group_ids = [
        yandex_vpc_security_group.vpc_security_group.id,
        yandex_vpc_security_group.vpc_security_group.id
      ]
    }

    resources {
      memory        = var.memory
      cores         = var.cores
      core_fraction = var.core_fraction
    }

    boot_disk {
      type = var.boot_disk_type
      size = var.boot_disk_size
    }

    scheduling_policy {
      preemptible = var.preemptible
    }

    container_runtime {
      type = var.container_runtime_type
    }
  }

  scale_policy {
    auto_scale {
      min     = var.scale_policy_min
      max     = var.scale_policy_max
      initial = var.scale_policy_initial
    }
  }

  allocation_policy {
    location {
      zone = var.zone
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "15:00"
      duration   = "3h"
    }

    maintenance_window {
      day        = "friday"
      start_time = "10:00"
      duration   = "4h30m"
    }
  }
}