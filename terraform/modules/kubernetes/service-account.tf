resource "yandex_iam_service_account" "k8_service_account" {
  name = var.k8_service_account_name
}

resource "yandex_resourcemanager_folder_iam_binding" "admin-binding" {
  folder_id = var.folder_id
  role      = "admin"
  members   = [
    "serviceAccount:${yandex_iam_service_account.k8_service_account.id}",
  ]
}

resource "yandex_resourcemanager_folder_iam_binding" "vpc-public-admin" {
  folder_id = var.folder_id
  role      = "editor"
  members   = [
    "serviceAccount:${yandex_iam_service_account.k8_service_account.id}"
  ]
}

resource "yandex_resourcemanager_folder_iam_member" "cluster_agent_role_for_SA" {
  folder_id = var.folder_id
  role      = "k8s.clusters.agent"
  member    = "serviceAccount:${yandex_iam_service_account.k8_service_account.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "vpc_public_admin_role_for_SA" {
  folder_id = var.folder_id
  role      = "vpc.publicAdmin"
  member    = "serviceAccount:${yandex_iam_service_account.k8_service_account.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "container_registry_images_puller_role_for_SA" {
  folder_id = var.folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.k8_service_account.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "kms_keys_encrypter_decrypter_role_for_SA" {
  folder_id = var.folder_id
  role      = "kms.keys.encrypterDecrypter"
  member    = "serviceAccount:${yandex_iam_service_account.k8_service_account.id}"
}