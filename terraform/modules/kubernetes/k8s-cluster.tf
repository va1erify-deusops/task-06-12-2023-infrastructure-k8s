resource "yandex_kubernetes_cluster" "k8_cluster" {
  name       = var.k8_cluster_name
  network_id = var.network_id

  master {
    zonal {
      zone      = var.subnetwork_zone
      subnet_id = var.subnetwork_id
    }
    public_ip          = var.k8_cluster_public_ip
    security_group_ids = [yandex_vpc_security_group.vpc_security_group.id]
  }

  service_account_id      = yandex_iam_service_account.k8_service_account.id
  node_service_account_id = yandex_iam_service_account.k8_service_account.id

  depends_on = [
    yandex_resourcemanager_folder_iam_member.cluster_agent_role_for_SA,
    yandex_resourcemanager_folder_iam_member.vpc_public_admin_role_for_SA,
    yandex_resourcemanager_folder_iam_member.container_registry_images_puller_role_for_SA,
    yandex_resourcemanager_folder_iam_member.kms_keys_encrypter_decrypter_role_for_SA
  ]

  kms_provider {
    key_id = yandex_kms_symmetric_key.kms_key.id
  }

}