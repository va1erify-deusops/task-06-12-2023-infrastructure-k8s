resource "yandex_iam_service_account" "s3_service_account" {
  name  = var.s3_service_account_name
}

resource "yandex_resourcemanager_folder_iam_member" "s3_service_account_role_to_folder" {
  folder_id = var.folder_id
  role      = var.s3_service_account_role
  member    = "serviceAccount:${yandex_iam_service_account.s3_service_account.id}"
}

resource "yandex_iam_service_account_static_access_key" "s3_service_account_static_key" {
  service_account_id = yandex_iam_service_account.s3_service_account.id
}


resource "yandex_storage_bucket" "s3_backend_storage_bucket" {
  access_key = yandex_iam_service_account_static_access_key.s3_service_account_static_key.access_key
  secret_key = yandex_iam_service_account_static_access_key.s3_service_account_static_key.secret_key
  bucket     = var.s3_bucket_name

  anonymous_access_flags {
    read        = var.s3_storage_bucket_read
    list        = var.s3_storage_bucket_list
    config_read = var.s3_storage_bucket_config_read
  }

  max_size      = var.s3_storage_bucket_max_size
  force_destroy = var.s3_storage_bucket_force_destroy

}