variable "token" {}
variable "cloud_id" {}
variable "folder_id" {}

variable "zone" {
  default = "ru-central1-a"
}

variable "network_name" {
  default = "kubernetes_network"
}
variable "subnetwork_name" {
  default = "kubernetes_subnetwork"
}
variable "cidr_v4" {
 default = "192.168.10.0/24"
}

variable "static_address_name" {
  default = "kubernetes_static_address_name"
}
variable "deletion_protection" {
  default = "false"
}

variable "k8_cluster_name" {
  default = "k8-cluster"
}
variable "k8_cluster_public_ip" {
  default = "true"
}
variable "k8_service_account_name" {
  default = "k8-service-account-name-test"
}
variable "k8s_node_group_name" {
  default = "k8-node-group"
}
variable "kms_key_default_algorithm" {
  default = "AES_128"
}
variable "kms_key_name" {
  default = "kms_key"
}
variable "kms_key_rotation_period" {
  default = "8760h"
}
variable "vpc_security_group_name" {
  default = "kubernetes_security_group"
}

variable "kubernetes_platform_id" {
  default = "standard-v3"
}

variable "kubernetes_nat" {
  default = true
}

variable "kubernetes_memory" {
  default = 4
}

variable "kubernetes_cores" {
  default = 2
}

variable "kubernetes_core_fraction" {
  default = 20
}

variable "kubernetes_boot_disk_type" {
  default = "network-hdd"
}

variable "kubernetes_boot_disk_size" {
  default = 30
}

variable "kubernetes_preemptible" {
  default = false
}

variable "kubernetes_container_runtime_type" {
  default = "containerd"
}

variable "kubernetes_scale_policy_min" {
  default = 1
}
variable "kubernetes_scale_policy_max" {
  default = 2
}
variable "kubernetes_scale_policy_initial" {
 default = 1
}
variable "vm_name" {
  default = "nfs-server"
}

variable "vm_internal_ip_address" {
  default = "192.168.10.12"
}

variable "vm_hostname" {
  default = "nfs-server"
}

variable "vm_platform" {
  default = "standard-v2"
  description = "Platform YCloud"
}

variable "vm_ram" {
  default = 0.5
  description = "Count RAM in GB"
}

variable "vm_cpu" {
  default = 2
  description = "Count CPU"
}

variable "vm_core_fraction" {
  default = 5
}

variable "vm_boot_disk_image_id" {
  default = "fd8t849k1aoosejtcicj"
}
variable "vm_boot_disk_size" {
  default = 5
}

variable "vm_boot_disk_type" {
  default = "network-hdd"
}

variable "vm_boot_disk_block_size" {
  default = 4096
}

